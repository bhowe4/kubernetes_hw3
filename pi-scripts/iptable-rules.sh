#!/bin/bash

sudo iptables -A FORWARD -i cni0 -j ACCEPT
sudo iptables -A FORWARD -o cni0 -j ACCEPT
sudo iptables -A FORWARD -i flannel.1 -j ACCEPT
sudo iptables -A FORWARD -o flannel.1 -j ACCEPT
