#!/bin/bash

hosts="$(pwd)/hosts.txt"
output_file="$(pwd)/host2ip.txt"

declare -A host_ip_map

while read hostname
do
    echo -n "discovering host: ${hostname} ... "
    host_ip=$(host ${hostname} | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')

    if [ -z ${host_ip} ]
    then
        echo "host not found"
    else
        echo "found at ${host_ip}"
        host_ip_map[${hostname}]=${host_ip}
    fi
done < "$hosts"

if [ -f ${output_file} ]
then
    rm ${output_file}
fi

for k in "${!host_ip_map[@]}"
do
    echo "${k} ${host_ip_map[$k]}" >> ${output_file}
done