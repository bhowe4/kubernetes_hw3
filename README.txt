To deploy hadoop in kubernetes on a raspberry pi cluster follow these steps.

1. flash all sd cards with the latest version of hypriotOS. The install-flash.sh will download
   a convenient tool also provided by hypriot that lets you set the host name and easily download
   and flash the image to the sd card.

2. Connect all pis to the same router as the computer that will be running the rest of the commands
   and make sure you can connect to all the devices. This can be done using discover-hosts.sh but
   this script doesn't work all the time so you may need to log into the router admin page or
   run an nmap scan to identify the pis ip addresses. After you find the ips put them in the host2ip.txt
   file next to the pis hostname. This file helps keep track of which pi is which and lets the other
   scripts easily find the pis.

3. Run prep-node.sh, this script will generate an sshkey install it, download and install kubernetes,
   and set the iptable rules (these aren't persistent though) on all the pis.

For running commands on the pis you can either ssh into them or use cluster-exec.sh

4. Now pick which pi will be the cluster master and run the following command 
    "kubeadm init --pod-network-cidr 10.244.0.0/16"
   which will initialize the master and print the kubeadm join command the you will need to run on
   all the slave nodes to connect them to the cluster. Also if you wish to run kubectl on the
   local machine copy the /etc/kubernetes/admin.conf file to the machine and set KUBECONFIG to
   the local location.

5. Next to set up the container network run the following
    "curl -sSL https://rawgit.com/coreos/flannel/v0.7.1/Documentation/kube-flannel-rbac.yml | kubectl create -f -"
    "curl -sSL https://rawgit.com/coreos/flannel/v0.7.1/Documentation/kube-flannel.yml | sed "s/amd64/arm/g" | kubectl create -f -"
   now if you run "kubectl get node" all of the nodes should say Ready. Also if you what the graphical dashboard
   which makes things easier to monitor run
    "curl -sSL https://rawgit.com/kubernetes/dashboard/master/src/deploy/kubernetes-dashboard.yaml | sed "s/amd64/arm/g" | kubectl create -f -"
    "kubectl -f kubernetes-configs/dashboard-admin.yaml"
   you can now run "kubectl proxy" and go to http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

6. Now to deploy hadoop. This is done using the htps://github.com/comcast/kube-yarn project which has been
   cloned into the kube-yarn folder in this repo and modified to work on arm architecture. This was done
   by creating an arm docker image for hadoop (kube-yarn/image/Dockerfile also uploaded to https://hub.docker.com/r/trajan1905/rpi-hadoop/) 
   then modifying the kubernetes manifests (kube-yarn/manifests/) to use this image and adjust the memory
   usage so they would work on the pis limited resources. To deploy hadoop all you have to do is run make
   in the kube-yarn directory and it should take care of everything.

7. To run a job on hadoop copy the input files and jar to the yarn-nm-0 container and add them to hdfs.
    "kubectl cp <input file> yarn-cluster/yarn-nm-0:/"
    "kubectl cp <jar file> yarn-cluster/yarn-nm-0:/"
    "kubectl exec -it yarn-cluster/yarn-nm-0 /bin/bash"
    "hdfs dfs -mkdir /input"
    "hdfs dfs -put <input file> /input/"
    "yarn jar <jar file> [args]"
    