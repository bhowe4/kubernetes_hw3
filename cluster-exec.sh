#!/bin/bash

declare -A host_ip_map

master="master"
user="pirate"
ssh_key="$(pwd)/keys/cluster-key"

while getopts iom:c: option
do
    case "${option}"
    in
    i) ignore_master=1;;
    o) master_only=1;;
    m) master=${OPTARG};;
    c) cmd=${OPTARG};;
    esac
done

if [ -z ${cmd} ]
then
    echo 'must provide a command'
    exit 1
fi

while read -ra line
do
    host_ip_map[${line[0]}]=${line[1]}
done < "host2ip.txt"

for k in "${!host_ip_map[@]}"
do

    if [ ! -z "${ignore_master}" ] && [ "${k}" == "${master}" ]
    then
        echo "skipping ${k}"
    elif [ ! -z "${master_only}" ] && [ ! "${k}" == "${master}" ]
    then
        echo "skipping ${k}"
    else
        host_ip=${host_ip_map[$k]}
        echo "executing on ${k} at ${host_ip}"

        ssh -i ${ssh_key} ${user}@${host_ip} "${cmd}"

        if [ $? == 0 ]
        then
            echo "success"
        else
            echo "failed"
        fi
    fi

    echo -e
done
