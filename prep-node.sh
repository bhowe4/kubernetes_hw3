#!/bin/bash

declare -A host_ip_map

user="pirate"
ssh_key="$(pwd)/keys/cluster-key"
init_script="$(pwd)/pi-scripts/init.sh"

while read -ra line
do
    host_ip_map[${line[0]}]=${line[1]}
done < "host2ip.txt"

if [ -f ${ssh_key} ]
then
    echo "Key found at ${ssh_key}"
else
    ssh-keygen -f ${ssh_key}
fi

for k in "${!host_ip_map[@]}"
do
    host_ip=${host_ip_map[$k]}
    echo "executing on ${k} at ${host_ip} ... "

    ssh-copy-id -i ${ssh_key} ${user}@${host_ip}

    if [ $? == 0 ]
    then
        echo "success"
        ssh -i ${ssh_key} ${user}@${host_ip} 'sudo -s' < ${init_script}
    else
        echo "failed"
    fi

    echo -e
done